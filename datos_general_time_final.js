//Esta query proporciona los tiempos y eventos 
//por aplicación para cada uno de los RLP
  db.app_cell_traffic.aggregate(
    [
      {$match: 
              { 
                 "identity.mcc": 214,
                 "start_day_local": {$gte:  "20171001", $lte:  "20171031"},

              }
      },
     {$unwind : "$apps"},
     {$group: {

               "_id": {"RLP":"$id_rel_line_plan",
               "appPackage": "$apps.app_package", 
               "totalTime": "$apps.duration_milliseconds", 
               "local_day": "$start_day_local"},             
               }
      },
      
      {$group: {
        "_id": {"appPackage": "$_id.appPackage", 
        "RLP":"$_id.RLP", "local_day": "$_id.local_day"}, 
        "totalTimeApp": { $sum: "$_id.totalTime" },
        "nEvents": {$sum: 1},
        "rlp_list": {$addToSet: "$_id.RLP" },
      }},

      {$match: 
              {
                "totalTimeApp": {$gt:0, $lte: 8.64e+7},
                //"nEvents": {$gte:2}
            } 
      },
      {$lookup: {
           from: "user_info",
           localField: "rlp_list",
           foreignField:  "id_rel_line_plan",
           as: "personal_data"
              }
      },
      {$unwind : "$personal_data"},
      {$project : 
               {
                   "_id":0,  
                   "RLP": "$_id.RLP", 
                   "appPackage": "$_id.appPackage",
                   "totalTime2": "$totalTimeApp",
                   "totalEvents": "$nEvents",
                   "local_day": "$_id.local_day",
                   "name": "$personal_data.name",
                   "gender": "$personal_data.gender",
                   "birthday": "$personal_data.birthday",
                   "age_range": "$personal_data.age_range",
                   "brand": "$personal_data.brand",
                   "model": "$personal_data.model",
                   "quota": "$personal_data.quota"
                } 
        },

      {$out: "cell_traffic_octoberc"}    
      ],{allowDiskUse: true}).pretty()
-------------------------------------------------------------------

//   mongo "mongodb://10.0.0.4:27017,10.0.0.7:27017,10.0.0.8:27017/weplan_partis?replicaSet=PROD-RS&socketTimeoutMS=3600000"

// app_cell_traffic  para sacar las apps por RLP

db.app_cell_traffic.aggregate(
  [
    {$match: 
              { 
                "identity.mcc": 214,
                "start_day_local": {$gte:  "20171002", $lt:  "20171003"},

    }
              },
    {$unwind : "$apps"},
    {$group: { "_id": {"RLP":"$id_rel_line_plan","appPackage": "$apps.app_package", "totalTime": "$apps.duration_milliseconds"},}
    },
    {$unwind : "$rlp_list"},
    {$project:
              {
              "_id":0,  
              "RLP": "$rlp_list", 
              "appPackage": "$_id.appPackage",
              "totalTime": "$totalTime.totalTime",
              } 
    },
    //{$out: "general_time"}    
    ],{allowDiskUse: true}).pretty()
      
--------------------------------------------------------------

db.app_cell_traffic.aggregate(
  [
    {$match: 
              { 
                "identity.mcc": 214,
                "start_day_local": {$gte:  "20171002", $lt:  "20171003"},

    }
              },
    {$unwind : "$apps"},
    //{$out: "general_time"}    
    ],{allowDiskUse: true}).pretty()

      